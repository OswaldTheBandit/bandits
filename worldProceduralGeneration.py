import worldObject
import random
import utilities
import islandGeneration
import regionGenerator
import technology
import skins
import math

def getSmoothValue(x, y, saturation, depth, landmap, smoothValueRange):
    total = 0
    for offsetX in smoothValueRange:
        for offsetY in smoothValueRange:
            total += landmap[offsetX + x][offsetY + y]
    if total > saturation:
        return 1
    else:
        return 0

def smoothLandMapCycle(oldmap, landmap, depth, saturation, world, smoothValueRange):
    for x in range(depth,  world.width - depth):
        for y in range(depth,  world.height - depth):
            landmap[x][y] = getSmoothValue(x, y, saturation, depth, oldmap, smoothValueRange)

def smoothLandMap(landmap, loops,  minimumSaturation,  world):
    repeat=1
    newMap = utilities.createMatrix(world.width, world.height)
    for saturation in minimumSaturation:
        smoothValueRange = range(-repeat, repeat + 1)
        for loopsNeeded in range(loops):
            print("%s : %s" % (repeat, loopsNeeded))
            smoothLandMapCycle(landmap, newMap, repeat, saturation, world, smoothValueRange)
            landmap = newMap
        repeat += 1
    return landmap

def getHigh(landmap, world):
    high = []
    for x in range(1, world.width-1):
        for y in range(1, world.height-1):
            if landmap[x][y] == 1:
                high.append((x, y))
    return high

offset = [(-1, 0), (1,0), (0, -1), (0,1)]
def surroundedByHigh(x, y, height, heightmap):
    surrounded = True
    for currentOffset in offset:
        if heightmap[x + currentOffset[0]][y + currentOffset[1]] < height:
            surrounded = False
            continue
    return surrounded
        
def getHeightLevel(current, rnd, chance):
    randomValue = rnd.random()
    if randomValue < chance[0] and current > 1:
        return current - 1
    elif randomValue < chance[1]:
        return current
    else:
        return current + 1

def averageHeight(heightmap, x, y):
    total = 0
    for current in offset:
        total += heightmap[x + current[0]][y + current[1]]
    total += heightmap[x][y]
    return round(float(total) / float(len(offset) + 1))

def heightmapFromLandmap(landmap,  world, rnd, chance):
    heightmap = utilities.copyMatrix(landmap)
    highmap = utilities.copyMatrix(landmap)
    high = getHigh(landmap, world)
    currentHeight = 0
    while len(high) > 0:
        currentHeight += 1
        higher = []
        for x in high:
            if surroundedByHigh(x[0], x[1], currentHeight, highmap):
                highmap[x[0]][x[1]] = currentHeight + 1
                averageHeightWanted = averageHeight(heightmap, x[0], x[1])
                weightedChance = [chance[0], chance[1]]
                if averageHeightWanted > 4:
                    weightedChance =[chance[0] + .1, chance[1] + .1]
                heightmap[x[0]][x[1]] = getHeightLevel(averageHeightWanted , rnd, weightedChance)
                higher.append(x)
        high = higher
    return heightmap

def removeFloaters(heightmap, world):
    possible = []
    def isFloater(x, y):
        if heightmap[x][y] == 0:
            possible.append((x, y))
            return 1
        else:
            return 0
        
    floatMap = utilities.createMatrix(world.width, world.width, lambda x, y: isFloater(x, y))
    total = len(possible)
    while total > 0:
        print(total)
        valid = []
        anyGone = False
        for x in range(total - 1, -1, -1):
            isHooked = False
            for current in offset:
                offsetX = possible[x][0] + current[0]
                offsetY = possible[x][1] + current[1]
                if floatMap[offsetX][offsetY] == 0 and heightmap[offsetX][offsetY] >= 0:
                    isHooked = True
                    break
            if isHooked:
                floatMap[possible[x][0]][possible[x][1]] = 0
                anyGone = True
            else:
                valid.append(possible[x])
        if anyGone == False:
            for x in valid:
                heightmap[x[0]][x[1]] = -1
            break
        possible = valid
        total = len(possible)
    return heightmap

def getHeightmap(landmap, world, rnd):
    landHeightmap = heightmapFromLandmap(landmap,  world, rnd, [.2, .5])
    watermap = utilities.createMatrix(world.width,  world.height, lambda x,  y: 0 if landmap[x][y] == 1 else 1)
    waterHeightmap = heightmapFromLandmap(watermap,  world, rnd, [.12, .4])
    heightmap = utilities.createMatrix(world.width,  world.height,  lambda x,  y: (-waterHeightmap[x][y]if waterHeightmap[x][y] != 0 else landHeightmap[x][y] - 1) + (1 if x >= 3 and x < world.width - 3 and  y >= 3 and y < world.height - 3 else 0 ))
    heightmap = removeFloaters(heightmap, world)
    world.heightmap = utilities.copyMatrix(heightmap)

def getIslandSize(islandMap, world, islandTotal):
    islandSize = [0] * islandTotal
    for x in range(world.width):
        for y in range(world.height):
            if islandMap[x][y] > 0:
                islandSize[islandMap[x][y]-1] += 1
    return islandSize

def addRegionsToWorld(world, regionTotal, islandTotal, islandSize, centers):
    for x in range(regionTotal):
        world.addTerritory(x)
    for x in range(islandTotal):
        world.addIsland(x, islandSize[x] , centers[x][0], centers[x][1])

def generateHeightAndLandMap(world, rnd):
    landmap = utilities.createMatrix(world.width,  world.height,  lambda x,  y: (1 if rnd.random() > .53 else 0) if x >= 3 and x < world.width - 3 and  y >= 3 and y < world.height - 3 else 0)
    landmap = smoothLandMap(landmap, 3, [4, 12], world) # 7 removed
    print("Getting heightmap from landmap")
    getHeightmap(landmap, world, rnd)
    landmap = utilities.createMatrix(world.width, world.height, lambda x, y: (0 if world.heightmap[x][y] < 0 else 1))
    return landmap

def generateRegionsAndIslands(world, landmap, regionsWanted):
    world.islandmap, islandTotal= islandGeneration.findIslands(landmap,  world)
    print("Number of islands %s" % islandTotal)
    islandSize = getIslandSize(world.islandmap, world, islandTotal)
    world.islandToRegion, regionMap, centers, regionTotal = regionGenerator.findRegions(world.islandmap, islandTotal, islandSize, world, regionsWanted)
    world.territorymap = regionMap
    print(world.islandToRegion)
    addRegionsToWorld(world, regionTotal, islandTotal, islandSize, centers)
    return regionTotal
    

def generate(mapSeed, width, height, arguements):
    print("Generating world")
    regionsWanted = (int(raw_input("Regions wanted: ")) if arguements.regionsWanted == None else arguements.regionsWanted)
    world = worldObject.World(width,  height)
    rnd = random.Random()
    rnd.seed((mapSeed, "mapBase"))
    #0 is sea 1 is land
    landmap = generateHeightAndLandMap(world, rnd)
    regionTotal = generateRegionsAndIslands(world, landmap, regionsWanted)
    tech = technology.getTechPoints(world.territory, mapSeed)
    world.skinHolder = skins.SkinHolder(4)
    for x in range(regionTotal):
        world.territory[x].setTech(worldObject.TechItems(world.skinHolder.skins[tech[x].skin], tech[x]))
    print(world.territory)
    return world
