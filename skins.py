import os
import worldObject

class SkinItem:
    def __str__(self):
        return "Name: " + self.name + ", Min: " + str(self.techMin) + ", Max: " + str(self.techMax) + ", Values: " + str(self.values)
    def __repr__(self):
        return str(self)
    def __init__(self, techMin, techMax, itemFile, name):
        self.techMin = techMin
        self.techMax = techMax
        self.values = dict()
        self.name = name
        script_dir = os.path.dirname(__file__) #<-- absolute dir the script is in
        assets = os.path.join(script_dir, "assets")
        skinFilePath = os.path.join(assets, itemFile)
        itemFile = open(skinFilePath)
        for line in itemFile:
            line = line.rstrip()
            key, value = line.split(":")
            self.values[key] = value
            

class Skin:
    itemCategories = ["melee", "armor", "ranged", "artillery", "boats", 
      "building", "trade", "mining", "craft", "farm", "training", "rallying"]
    def __repr__(self):
        return str(self.items) + "\n"
    def __str__(self):
        return str(self.items)
    def __init__(self, index, skinFile):
        self.index = index
        script_dir = os.path.dirname(__file__) #<-- absolute dir the script is in
        assets = os.path.join(script_dir, "assets")
        skinFilePath = os.path.join(assets, skinFile)
        skinInfo = open(skinFilePath)
        self.items=dict()
        for x in self.itemCategories:
            self.items[x] = []
        for line in skinInfo:
            line = line.rstrip()
            itemType, name, techMin, techMax, fileLocation = line.split(":")
            self.items[itemType].append(SkinItem(int(techMin), int(techMax), fileLocation, name))

class SkinHolder:
    skins = []
    def __str__(self):
        return str(self.skins)
    def __init__(self):
        return holder
    def __init__(self, totalSkins):
        script_dir = os.path.dirname(__file__) #<-- absolute dir the script is in
        assets = os.path.join(script_dir, "assets")
        for x in range(totalSkins):
            skinFilePath = str(x) + ".skinData"
            self.skins.append(Skin(x, skinFilePath))

def getValidItems(skin, techValue, name):
    valid = []
    possible = skin.items[name]
    for x in possible:
        if techValue >= x.techMin and techValue <= x.techMax:
            valid.append(x)
    return valid
    