import hashlib
import worldProceduralGeneration
import worldObject
import random
import string
from numpy import array
import utilities
from PIL import Image as png
import os
import settlementGenerator
import landStats
import countyGenerator
import simulateTime

def colorFromHeight(x):
    rgb = []
    if x < 0:
        rgb = [0, 0, 255]
    elif x <= 255:
        rgb = [0, x, 255 - x]
    elif x > 340:
        rgb = [255, 0, 0]
    else:
        rgb = [(x - 256) * 3, max(255 - (x - 256) * 3, 0), 0]
    return rgb

#modified version taken from http://stackoverflow.com/questions/2030053/random-strings-in-python
def randomword(length):
    rnd = random.Random()
    return ''.join(rnd.choice(string.ascii_letters) for i in range(length))

def createWorldRandomSeed(arguements):
    createWorldFromSeed(randomword(10), arguements)

def printOpenmap(newWorld, script_dir):
    islandmapImage= utilities.colorMatrix(newWorld.width,  newWorld.height,  (lambda x, y: utilities.getColor(newWorld.openMap[x][y])))
    png.fromarray(islandmapImage, 'RGB').save(os.path.join(script_dir, 'openmap.png'))

def printHeightmap(newWorld, script_dir):
    heightmap = utilities.colorMatrix(newWorld.width,  newWorld.height,  (lambda x, y: colorFromHeight((newWorld.heightmap[x][y])*16 + 256)))
    png.fromarray(heightmap, 'RGB').save(os.path.join(script_dir, 'worldHeightmap.png'))
    
def printFertilitymap(newWorld, script_dir):
    fertmap = utilities.colorMatrix(newWorld.width,  newWorld.height,  (lambda x, y: colorFromHeight((newWorld.fertility[x][y]) + 84)))
    png.fromarray(fertmap, 'RGB').save(os.path.join(script_dir, 'worldFertilitymap.png'))

def colorForMaterialMap(material):
    if material == 0:
        return [255, 255, 0]
    if material == 1:
        return [0, 255, 0]
    if material == 2:
        return [0, 255, 255]
    if material == 3:
        return [0, 0, 255]
    return [255, 0, 0]

def printMaterialmap(newWorld, script_dir):
    materialmap = utilities.colorMatrix(newWorld.width,  newWorld.height,  (lambda x, y: [0, 0, 0] if newWorld.heightmap[x][y] < 0 else colorForMaterialMap(newWorld.ground[x][y])))
    png.fromarray(materialmap, 'RGB').save(os.path.join(script_dir, 'worldMaterialmap.png'))

def printLandmap(newWorld, script_dir):
    landmap = utilities.createMatrix(newWorld.width,  newWorld.height,  (lambda x, y: int(0 if newWorld.heightmap[x][y] < 0 else 255)))
    png.fromarray(array(landmap, dtype="uint8"), 'L').save(os.path.join(script_dir, 'worldLandmap.png'))

def getRainColor(x, y, world):
    value = world.rain[x][y]
    if value == -1:
        return [0 ,0 ,0]
    elif value == 0:
        return [255, 0, 255]
    elif value == 1:
        return [255, 255, 0]
    elif value == 2:
        return [0, 255, 255]
    elif value <= 5:
        rgb = [0] * 3
        rgb[5 - value] = 255
        return rgb

def printRainmap(newWorld, script_dir):
    rainmap = utilities.colorMatrix(newWorld.width,  newWorld.height,  (lambda x, y: getRainColor(x, y, newWorld)))
    png.fromarray(rainmap, 'RGB').save(os.path.join(script_dir, 'worldRainmap.png'))
    
def printPortmap(newWorld, script_dir):
    islandmapImage= utilities.colorMatrix(newWorld.width,  newWorld.height,  (lambda x, y: [0, 255, 0] if (x, y) in newWorld.tradeGraph else ([255, 0, 255] if newWorld.openMap[x][y] == -1 else utilities.getColor(newWorld.islandmapLakesMarked[x][y]))))
    png.fromarray(islandmapImage, 'RGB').save(os.path.join(script_dir, 'portmap.png'))
def printIslandmap(newWorld, script_dir):
    islandmapImage= utilities.colorMatrix(newWorld.width,  newWorld.height,  (lambda x, y: utilities.getColor(newWorld.islandmapLakesMarked[x][y])))
    png.fromarray(islandmapImage, 'RGB').save(os.path.join(script_dir, 'islandmap.png'))
    
def printSkinmap(newWorld, script_dir):
    regionmapImage= utilities.colorMatrix(newWorld.width,  newWorld.height,  (lambda x, y: utilities.getColor((0 if newWorld.territorymap[x][y] == 0 else newWorld.territory[newWorld.territorymap[x][y]-1].tech.skin.index + 1))))
    png.fromarray(regionmapImage, 'RGB').save(os.path.join(script_dir, 'skinmap.png'))
    
def printTerritorymap(newWorld, script_dir):
    regionmapImage= utilities.colorMatrix(newWorld.width,  newWorld.height,  (lambda x, y: utilities.getColor(newWorld.territorymap[x][y])))
    png.fromarray(regionmapImage, 'RGB').save(os.path.join(script_dir, 'regionmap.png'))
    
def printCountymap(newWorld, script_dir):
    regionmapImage= utilities.colorMatrix(newWorld.width,  newWorld.height,  (lambda x, y: utilities.getColor(newWorld.countymap[x][y])))
    png.fromarray(regionmapImage, 'RGB').save(os.path.join(script_dir, 'countymap.png'))
    
def printSizemap(newWorld, script_dir):
    regionmapImage= utilities.colorMatrix(newWorld.width,  newWorld.height,  (lambda x, y: utilities.getColor((0 if newWorld.islandmap[x][y] == 0 else newWorld.islands[newWorld.islandmap[x][y]-1].size / 100 + 1))))
    png.fromarray(regionmapImage, 'RGB').save(os.path.join(script_dir, 'sizemap.png'))
    
def printSettlementmap(newWorld, script_dir):
    landmapImage= utilities.colorMatrix(newWorld.width,  newWorld.height,  (lambda x, y: utilities.getColor(4 + newWorld.islandmap[x][y]) if newWorld.heightmap[x][y] >= 0 else [0, 0, 0]))
    offsets = [(0, 0), (0, 1), (1, 0), (0, -1), (-1, 0)]
    for current in newWorld.settlements:
        for connection in current.connections:
            for location in connection.path:
                landmapImage[location[0]][location[1]] = [255, 255, 0]
    for current in newWorld.settlements:
        color = ([255, 0, 0] if current.fishing == False else [0, 0, 255]) if current.port == False else [0, 255, 0]
        for offset in offsets:
            landmapImage[current.x + offset[0]][current.y + offset[1]] = color
    for current in newWorld.settlements:
        for farm in current.farms:
            landmapImage[farm.x][farm.y] = [255, 0, 255]
    png.fromarray(landmapImage, 'RGB').save(os.path.join(script_dir, 'settlementmap.png'))
def printDebugMaps(newWorld, arguements):
    script_dir = os.path.dirname(__file__) #<-- absolute dir the script is in
    toPrint = arguements.toPrint
    print(toPrint)
    if "heightmap" in toPrint:
        printHeightmap(newWorld, script_dir)
    if "landmap" in toPrint:
        printLandmap(newWorld, script_dir)
    if "islandmap" in toPrint:
        printIslandmap(newWorld, script_dir)
    if "territorymap" in toPrint:
        printTerritorymap(newWorld, script_dir)
    if "skinmap" in toPrint:
        printSkinmap(newWorld, script_dir)
    if "sizemap" in toPrint:
        printSizemap(newWorld, script_dir)
    if "settlementmap" in toPrint:
        printSettlementmap(newWorld, script_dir)
    if "rainmap" in toPrint:
        printRainmap(newWorld, script_dir)
    if "materialmap" in toPrint:
        printMaterialmap(newWorld, script_dir)
    if "fertilitymap" in toPrint:
        printFertilitymap(newWorld, script_dir)
    if "openmap" in toPrint:
        printOpenmap(newWorld, script_dir)
    if "portmap" in toPrint:
        printPortmap(newWorld, script_dir)
    if "countymap" in toPrint:
        printCountymap(newWorld, script_dir)
    

def createWorldFromSeed( seed , arguements):
    print("Creating world with seed (%s)" % (seed))
    width = (raw_input("Enter width: ") if arguements.width == None else arguements.width)
    height = (raw_input("Enter height: ") if arguements.height == None else arguements.height)
    newWorld = worldProceduralGeneration.generate(seed.encode('utf-8'), int(width), int(height), arguements)
    settlementGenerator.addSettlementsForWorld(newWorld, seed)
    countyGenerator.getCountymap(newWorld)
    landStats.rainFrequency(newWorld)
    landStats.groundMaterial(newWorld)
    landStats.landFertility(newWorld, seed)
    countyGenerator.buildRoads(newWorld)
    print "broadly simulating years"
    simulateTime.broadlySimulateYears(newWorld, 80, seed)
    print newWorld.settlements
    print("Printing debug maps")
    printDebugMaps(newWorld, arguements)
    for x in newWorld.territory:
        print("%s:%s" % (x.size, len(x.islands)))
    print("DONE")
    return newWorld
