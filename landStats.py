import utilities
import random
#various functions for stuff like land fertility, rain frequency, and ground

offset = [(0,1), (1,0), (-1,0), (0,-1)]

deepOffset = [(0,1),(1,1), (1,0),(1,-1), (-1,0),(-1,-1), (0,-1), (-1,1)]

def numberHigher(x, y, world):
    total = 0
    for current in offset:
        offsetX = x + current[0]
        offsetY = y + current[1]
        if world.heightmap[x][y] <= world.heightmap[offsetX][offsetY]:
            total += 1
    return total
    
def getRainValue(x, y, world): 
    if world.islandmap[x][y] == 0 or x == 0 or y == 0 or x == world.width - 1 or y == world.height - 1:
        return -1
    rainValue = numberHigher(x, y, world)
    if world.heightmap[x][y] == 2:
        rainValue += 1
    elif world.heightmap[x][y] == 1:
        rainValue += 2
    rainValue = min(rainValue, 5)
    return rainValue

def rainFrequency(world):
    rain = utilities.createMatrix(world.width, world.height, lambda x, y: getRainValue(x, y, world))
    world.rain = rain
    
def touchesOcean(x, y, world):
    for current in deepOffset:
        offsetX = x + current[0]
        offsetY = y + current[1]
        if world.heightmap[x][y] < 0:
            return True
    return False

#sand and soil may be changed if settlement exists on settlement gen
#3 rock, 2 rocky, 1 soil, 0 sand
def getApproximateGroundMaterial(x, y, world):
    if x == 0 or y == 0 or x == world.width - 1 or y == world.height - 1:
        return 0
    higher = numberHigher(x, y, world)
    if touchesOcean(x, y, world) or world.heightmap[x][y] == 0:
        return 0
    elif higher  == 0 or (world.heightmap[x][y] >= 5 and higher <= 1) or world.heightmap[x][y] >= 8:
        return 3
    elif higher  <= 1 or (world.heightmap[x][y] >= 3 and higher <= 2) or world.heightmap[x][y] >= 6:
        return 2
    else:
        return 1

def groundMaterial(world):
    ground = utilities.createMatrix(world.width, world.height, lambda x, y: getApproximateGroundMaterial(x, y, world))
    world.ground = ground
    
def getLandFertility(x, y, world, rnd):
    if world.islandmap[x][y] == 0 or x == 0 or y == 0 or x == world.width - 1 or y == world.height - 1 :
        return 0
    material = world.ground[x][y]
    rain = world.rain[x][y]
    fertility = 0
    if material == 1:
        fertility += 7
    elif material == 3:
        fertility += 2
    elif material == 2:
        fertility += 4
    
    fertility += rain
    fertility *= 19
    fertility += rnd.randint(-9, 9)
    max(fertility, 0)
    return fertility

def landFertility(world, seed):
    rnd = random.Random()
    rnd.seed((seed, "fertility"))
    fertility = utilities.createMatrix(world.width, world.height, lambda x, y: getLandFertility(x, y, world, rnd))
    world.fertility = fertility
    