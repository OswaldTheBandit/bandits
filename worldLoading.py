import os
import worldCreation

def getWorld(arguements):
    print("Initializing resources")

    #get world list
    
    script_dir = os.path.dirname(__file__) #<-- absolute dir the script is in
    if os.path.isfile(os.path.join(script_dir, "worlds.txt")) == False:
        #create new worlds.txt file
        newWorldFile=os.open(os.path.join(script_dir, "worlds.txt"),  os.O_RDWR|os.O_CREAT)
        os.close(newWorldFile)

    #load world names
    #code for reading to list taken from 
    #http://stackoverflow.com/questions/3925614/how-do-you-read-a-file-into-a-list-in-python
    worldNames = [line.strip() for line in open(os.path.join(script_dir, "worlds.txt") , 'r')]
    print("Names loaded")
    if len(worldNames) == 0:
        print("Need to create new world")
        createWorld(arguements)
    else:
        print("Time to load world or create new")
        input = raw_input("Do you want to (C)reate new world or (L)oad world: ")
        if input == "C":
            createWorld();
        elif input == "L":
            loadWorld()
        else:
            print("Invalid input, terminating")
        

def loadWorld():
    print("Load world")

def createWorld(arguements):
    if arguements.seed != None:
        worldCreation.createWorldFromSeed(arguements.seed, arguements)
    elif arguements.random:
        worldCreation.createWorldRandomSeed(arguements)
    else:
        chooseOrRandom = None
        while chooseOrRandom != "C" and chooseOrRandom != "R":
            chooseOrRandom = raw_input("(C)hoose seed or (R)andom seed: ")
            if chooseOrRandom != "C" and chooseOrRandom != "R":
                print("Invalid input, try again.")
        if chooseOrRandom == "C":
            seed = str(input("Enter seed: "))
            worldCreation.createWorldFromSeed(seed, arguements)
        else:
            worldCreation.createWorldRandomSeed(arguements)
