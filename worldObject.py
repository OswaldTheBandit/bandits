import math
import skins

class HumanUnit:
    def __str__(self):
        return ("Units: %s, Optimal %s" % (self.units, self.optimalSize))
    def __repr__(self):
        return str(self)
    totalIds = 0
    def setId(self, wanted, training = None, optimal = 0):
        self.unitId = HumanUnit.totalIds
        HumanUnit.totalIds += 1
        self.units = wanted
        self.optimalSize = wanted if optimal == 0 else optimal
        self.totalPeople = 0
        self.people = dict()
        self.training = training
    def moveSquad(self, to):
        to.addMore(self.units, self.optimalSize, self.people)
        
class UnitsGrouped:
    def __init__(self, typeWanted):
        if not issubclass(typeWanted, HumanUnit):
            print "ERROR"
        self.current = typeWanted
        self.squads = []
        self.total = 0
        self.retrain = 0
        self.totalPeople = 0
        self.totalSkill = 0
     
    def trainAll(self, moreSkill):
        for x in self.squads:
            x.giveSkill(moreSkill) 
    
    def addSquad(self, size, training = None, optimal = 0):
        newSquad = self.current(size, training, optimal)
        self.squads.append(newSquad)
        self.total += size
        return newSquad
    
    def moveSquad(self, squad, to):
        squad.moveSquad(to)
        self.total -= squad.units
        self.removeBlankSquad(squad)
    
    
    def addMore(self, toAdd, squadSize, training = None, people = dict()):
        added = 0
        if toAdd <= 0:
            return 0
        if self.retrain > 0:
            toTrain = min(self.retrain, toAdd)
            toAdd -= toTrain
            added += toTrain
            self.retrainUnits(toTrain, people)
        squadsToAdd = int(math.floor(toAdd / squadSize))
        for x in range(squadsToAdd):
            self.addSquad(squadSize, training, squadSize)
            toAdd -= squadSize
            added += squadSize
        if toAdd > 0:
            self.addSquad(toAdd, training, squadSize)
            added += toAdd
        return added
        
    def addMoreFrom(self, toAdd, squadSize, comeFrom):
        if toAdd < comeFrom.total - comeFrom.totalPeople:
            self.addMore(toAdd, squadSize)
            comeFrom.directKill(toAdd)
    
    def retrainUnits(self, toTrain, people = dict()):
        for x in self.squads:
            if toTrain == 0:
                break
            if x.units < x.optimalSize:
                needed = x.optimalSize - x.units
                canTrain = min(needed, toTrain)
                x.retrain(canTrain, people)
                self.total += canTrain
    def lowerTotal(self, newPercent):
        kill = math.floor(self.total * (1.0 - newPercent))
        for x in self.squads:
            if self.total == 0:
                break
            fraction = float(x.units) / float(self.total)
            goKill = int(min(math.ceil(fraction * kill), x.units - len(x.people)))
            x.units -= goKill
            self.retrain += goKill
            self.total -= goKill
            if x.units == 0:
                self.removeBlankSquad(x)
            
    def removeBlankSquad(self, squad):
        self.squads.remove(squad)
        self.retrain -= squad.optimalSize
            
    def directKill(self, kill):
        for x in self.squads:
            if kill <= 0:
                return
            goKill = int(min(kill, x.units - len(x.people)))
            kill -= goKill
            x.units -= goKill
            self.retrain += goKill
            self.total -= goKill
            if x.units == 0:
                self.removeBlankSquad(x)
                
        

class Worker(HumanUnit):
    def create(self, wanted, training = None, optimal = 0):
        self.setId(wanted, training, optimal)
        self.skill = 0.0
        self.training = training
    def retrain(self, trained, people):
        self.skill = (self.units * self.skill) / (float(trained) + float(self.units))
        self.units += trained
        soFar = 0
        for x in people:
            if soFar >= trained:
                break
            key, value = x
            self.people[key] = value
            del people[key]
    def giveSkill(self, skill):
        self.skill = min(self.skill + skill, 50)

class Peasant(Worker):
    def __init__(self, wanted, training = None, optimal = 0):
        self.create(wanted, training, optimal)
        
class Fishermen(Worker):
    def __init__(self, wanted, training = None, optimal = 0):
        self.create(wanted, training, optimal)
        
class Craftsmen(Worker):
    def __init__(self, wanted, training = None, optimal = 0):
        self.create(wanted, training, optimal)
        
class Trader(Worker):
    def __init__(self, wanted, training = None, optimal = 0):
        self.create(wanted, training, optimal)
        
class Scum(HumanUnit):
    def __init__(self, wanted, training = None, optimal = 0):
        self.setId(wanted, training, optimal)
    def retrain(self, total, people):
        self.units += total

class Connection:
    def __init__(self, start, end, path):
        self.start = start
        self.end = end
        self.path = path
        self.distance = len(path)

class Structure:
    totalIds = 0
    def create(self, x, y):
        self.x = x
        self.y = y
        self.structureId = Structure.totalIds
        Structure.totalIds += 1
        
class Stronghold(Structure):
    def __init__(self, x, y):
        self.create(x, y)
        self.army = 0
        self.squads = set()

class Farm(Structure):
    def __init__(self, x, y, size, yearYield, crop, peasantsUsed):
        self.create(x, y)
        self.crop = crop
        self.yearYield = yearYield
        Farm.totalIds += 1
        self.size = size
        self.workers = peasantsUsed

class Settlement:
    totalIds = 0
    def getTerritory(self):
        return self.island.territory
    def addConnection(self, end, path):
        self.connections.append(Connection(self, end, path))
        path.reverse()
        end.connections.append(Connection(end, self, path))
    def __str__(self):
        return ("X: %s, Y: %s, port: %s, fishing: %s, countySize: %s, population: %s, popgrow: %s, crime: %s, morale: %s, squalor: %s, buildings: %s, wealth: %s, food (%s, %s, %s), jobs(fish %s, trade %s, craft %s, peasants %s, scum %s) foodSources (fish %s, farm %s, trade %s) connect %s\n" % (self.x, self.y, self.port, self.fishing, self.countySize, self.population, self.popGrow, self.crime, self.morale, self.squalor, self.buildings, self.wealth, self.foodOwned, self.foodInput, self.foodNeed, self.fishermen.total, self.traders.total, self.craftsmen.total, self.peasants.total, self.scum.total, self.fishFood, self.farmFood, self.tradeFood, len(self.connections)))
    def __repr__(self):
        return str(self)
    def __init__(self, x, y, island, port, fishing):
        self.dontfarm = set()
        self.countyId = Settlement.totalIds
        Settlement.totalIds += 1
        self.fishFood = 0
        self.farmFood = 0
        self.tradeFood = 0
        self.x = x
        self.y = y
        self.countySize = 0
        self.island = island
        self.port = port
        self.fishing = fishing
        self.population = 500
        self.popGrow = 1
        self.buildings = 10
        self.crime = 500
        self.morale = 500
        self.squalor = 1000
        self.lastDeficit = 0
        self.trade = 0
        self.wealth = 0
        self.buildingType = None
        self.done = set()
        self.guild = None
        self.guildSize = 0
        self.foodInput = 0
        self.foodNeed = 500 * 360
        self.foodOwned = 0
        self.landNeighbours = set()
        self.connections = []
        self.pathOut = []
        self.farms = []
        self.top = 0
        self.bottom = 0
        self.left = 0
        self.right = 0
        self.farmxy = []
        self.state = "poor"
        self.fishermen = UnitsGrouped(Fishermen)
        self.traders = UnitsGrouped(Trader)
        self.craftsmen = UnitsGrouped(Craftsmen)
        self.peasants = UnitsGrouped(Peasant)
        self.scum = UnitsGrouped(Scum)
        self.scum.addMore(self.population, 1000)
        self.trade = None
        self.units = set()

class Island:
    def __repr__(self):
        return "x:" + str(self.x) +", y:" + str(self.y) + ", size:" + str(self.size)
    def __str__(self):
        return self.__repr__()
    def __init__(self, islandId, size, x, y):
        self.islandId = islandId
        self.x = x
        self.y = y
        self.size = size
        self.settlements = []
        self.territory = 0
"""
["melee", "armor", "ranged", "artillery", "boats", 
      "building", "trade", "mining", "craft", "farm", "training", "rallying"]
"""
class TechItems:
    def techValuesFromTech(self, tech):
        self.techValue["melee"] = tech.meleeWeapons
        self.techValue["armor"] = tech.armor
        self.techValue["ranged"] = tech.rangedWeapons
        self.techValue["artillery"] = tech.artillery
        self.techValue["boats"] = tech.boats
        self.techValue["building"] = tech.building
        self.techValue["trade"] = tech.trade
        self.techValue["mining"] = tech.mining
        self.techValue["craft"] = tech.craft
        self.techValue["farm"] = tech.farm
        self.techValue["training"] = tech.training
        self.techValue["rallying"] = tech.rallying
    def __init__(self, skin, tech):
        types = ["melee", "armor", "ranged", "artillery", "boats", 
          "building", "trade", "mining", "craft", "farm", "training", "rallying"]
        self.techValue = dict()
        self.items = dict()
        self.skin = skin
        self.techValuesFromTech(tech)
        for x in types:
            self.items[x] = skins.getValidItems(self.skin, self.techValue[x], x)

class Territory:
    def __repr__(self):
        return "Size " + str(self.size) + ", islands " + str(self.islands) + "\n"
    def __str__(self):
        return self.__repr__()
    def addIsland(self, island):
        self.islands.append(island)
        self.x = (self.x * self.size + island.x * island.size)/(island.size + self.size)
        self.y = (self.y * self.size + island.y * island.size)/(island.size + self.size)
        self.size += island.size
        island.territory = self
    #territoryId starts at 1
    def setTech(self, tech):
        self.tech = tech
    def __init__(self, territoryId, size, x, y):
        self.tech = None
        self.territoryId = territoryId
        self.settlements = []
        self.size = size
        self.islands = []
        self.x = x
        self.y = y

class World:
    def setSettlements(self, settlements):
        for x in settlements:
            current = Settlement(x.x, x.y, x.island, x.port, x.fishing)
            self.settlements.append(current)
            self.territoryFromIsland(current.island).settlements.append(current)
            current.island.settlements.append(current)
            self.xySettlements[(x.x, x.y)] = self.settlements[len(self.settlements) - 1]
    def territoryFromIsland(self, island):
        islandId = island.islandId
        territoryId = self.islandToRegion[islandId]
        return self.territory[territoryId]
    def addIsland(self, islandId, size, x, y):
        island = Island(islandId, size, x, y)
        self.islands.append(island)
        self.territory[self.islandToRegion[islandId]].addIsland(island)
    def addTerritory(self, territoryId):
        self.territory.append(Territory(territoryId, 0, 0, 0))
    def setHeightmap(self,  x,  y,  value):
        self.heightmap[x][y] = value
    def getHeightmap(self,  x,  y):
        return self.heightmap[x][y]
    def __init__(self, width,  height):
        self.tradeGraph = dict()
        self.width = width
        self.height = height
        self.countymap = []
        self.heightmap = []
        self.territorymap = [] 
        self.countymap = []
        self.territory = []
        self.islands = []
        self.islandToRegion = []
        self.islandmap = []
        self.islandmapLakesMarked = []
        self.ground = []
        self.rain = []
        self.fertility = []
        self.structures = []
        self.xySettlements = dict()
        self.settlements = []
        #past here unimplimented
        self.tileWidth = 33
        self.tileHeight = 33
