import worldObject
import random
from ordered_set import OrderedSet
import utilities
from Queue import *
import math

class Settlement:
    def __repr__(self):
        return "x: " + str(self.x) + ",y: " + str(self.y) + ", id:" + str(self.island.islandId) + (" PORT " if self.port else "") + (" FISH " if self.fishing else "")
    def __str__(self):
        return self.__repr__()
    def __init__(self, x, y, island, isPort, hasFishing):
        self.x = x
        self.y = y
        self.island = island
        self.port = isPort
        self.fishing = hasFishing
        self.pathOut = []

def getClosestIsland(world, islandMapId, x, y):
    offsets = [(0, 1), (1, 0), (0, -1), (-1, 0)]
    pointsToCheck = OrderedSet()
    pointsToCheck.add((x, y))
    current = 0
    while current < len(pointsToCheck):
        currentX, currentY = pointsToCheck[current]
        if currentX >= 0 and currentX < world.width and currentY >= 0 and currentY < world.height and world.islandmapLakesMarked[currentX][currentY] == islandMapId:
            return (currentX, currentY)
        else:
            for offset in offsets:
                pointsToCheck.add((currentX + offset[0], currentY + offset[1]))
        current += 1
    print("ERROR ERROR ERROR ERROR ERROR")
    return (0,0)

def checkPortAndFishing(world, x, y):
    offsets = [(0, 1), (1, 0), (0, -1), (-1, 0)]
    fishing = False
    port = False
    for offset in offsets:
        if world.islandmapLakesMarked[x + offset[0]][y + offset[1]] == 0:
            return (True, True)
        elif world.islandmap[x + offset[0]][y + offset[1]] == 0:
            fishing = True
    return (port, fishing)
    
def touchesOcean(oceanMarked, x, y, wanted):
    offsets = [(0, 1), (1, 0), (0, -1), (-1, 0)]
    for offset in offsets:
        if oceanMarked[x + offset[0]][y + offset[1]] == wanted:
            return True
    return False

def getSettlementLocation(x, y, island, world, forcePort=False): 
    if world.islandmapLakesMarked[x][y] == island.islandId + 1:
        if forcePort:
        #    print(str(island.islandId) + " force port")
            territory = world.territoryFromIsland(island)
            #territory coords and island coords averaged so port will face inwards toward the territory
            weight = 2
            waterX, waterY = getClosestIsland(world, 0, x, y)
            x, y = getClosestIsland(world, island.islandId + 1, waterX, waterY)
            return (x, y, True, True)
        else:
            #print(str(island.islandId) + " easy capital")
            port, fishing = checkPortAndFishing(world, x, y)
            return (x, y, port, fishing)
    else: 
     #   print(str(island.islandId) + " Getting closest")
        settleX, settleY = getClosestIsland(world, island.islandId + 1, x, y)
        port, fishing = checkPortAndFishing(world, settleX, settleY)
        return (settleX, settleY, port, fishing)

def getCapitalLocation(island, world, forcePort = False):
    if world.islandmapLakesMarked[island.x][island.y] == island.islandId + 1:
        if forcePort:
        #    print(str(island.islandId) + " force port")
            territory = world.territoryFromIsland(island)
            #territory coords and island coords averaged so port will face inwards toward the territory
            weight = 2
            waterX, waterY = getClosestIsland(world, 0, (island.x * weight + territory.x) / (weight + 1), (island.y * weight + territory.y) / (weight + 1))
            x, y = getClosestIsland(world, island.islandId + 1, waterX, waterY)
            return (x, y, True, True)
        else:
            #print(str(island.islandId) + " easy capital")
            port, fishing = checkPortAndFishing(world, island.x, island.y)
            return (island.x, island.y, port, fishing)
    else: 
     #   print(str(island.islandId) + " Getting closest")
        x, y = getClosestIsland(world, island.islandId + 1, island.x, island.y)
        port, fishing = checkPortAndFishing(world, island.x, island.y)
        return (x, y, port, fishing)

def doesRowContainIsland(world, islandMapId, row, start, finish):
    for x in range(start, finish + 1):
        if world.islandmap[x][row] == islandMapId:
            return True
    return False

def doesColumnContainIsland(world, islandMapId, column, start, finish):
    for x in range(start, finish + 1):
        if world.islandmap[column][x] == islandMapId:
            return True
    return False

def islandDimensions(world, island):
    count = 0
    while doesRowContainIsland(world, island.islandId + 1, island.y - count, 0, world.width - 1):
        count += 1
    top = island.y - count
    count = 0
    while doesRowContainIsland(world, island.islandId + 1, island.y + count, 0, world.width - 1):
        count += 1
    bottom = island.y + count
    count = 0
    while doesColumnContainIsland(world, island.islandId + 1, island.x - count, top, bottom):
        count += 1
    left = island.x - count
    count = 0
    while doesColumnContainIsland(world, island.islandId + 1, island.x + count, top, bottom):
        count += 1
    right = island.x + count
    return (top, bottom, left, right)


def getSettlementXY(world, island, settlements, forcePort, maxDistance, maxXDistance, maxYDistance, rnd):
    startX = rnd.randint(island.left, island.right)
    startY = rnd.randint(island.top, island.bottom)
    startX, startY = getClosestIsland(world, island.islandId + 1, startX, startY)
    needMoveX = 0
    needMoveY = 0
    count = len(settlements)
    for other in settlements:
        if other.x == startX and other.y == startY:
            startX += -1 if startX > island.x else 1
            startY += -1 if startY > island.y else 1
        offsetX = other.x - startX
        offsetY = other.y - startY
        distance = abs(offsetX) + abs(offsetY)
        needMoveX -= int( offsetX * (1.0 / float(distance)) * (float((maxDistance + maxXDistance) / 10.0) / float(count)))
        needMoveY -= int( offsetY * (1.0 / float(distance)) * (float((maxDistance + maxYDistance) / 10.0) / float(count)))
    startX += needMoveX
    startY += needMoveY
    if startX < island.left:
        startX = island.left
    elif startX > island.right:
        startX = island.right
    if startY < island.top:
        startY = island.top
    elif startY > island.bottom:
        startY = island.bottom
    return (startX, startY)

def pruneSettlements(settlements, clear):
    threshold = 64
    remove = []
    for x in range(len(settlements) - 1, clear, -1):
        current = settlements[x]
        smallest = 1000000
        for y in range(x - 1, -1, -1):
            xDistance = abs(settlements[x].x - settlements[y].x)
            yDistance = abs(settlements[x].y - settlements[y].y)
            distance = xDistance * xDistance + yDistance * yDistance
            if distance <= threshold:
                remove.append(x)
                break
    count = 0
    lastRemoved = -1
    for x in remove: 
        lastRemoved = x
        settlements.pop(x)
        count += 1
    return count

def addSettlementsTilWanted(world, island, rnd, settlementsWanted, settlements, hasPort):
    maxXDistance = island.bottom - island.top
    maxYDistance = island.right - island.left
    maxDistance = max(maxXDistance, maxYDistance)
    count = 0
    for current in range(len(settlements), settlementsWanted):
        forcePort = False
        if current == settlementsWanted - 1 and hasPort == False:
            forcePort = True
        startX, startY = getSettlementXY(world, island, settlements, forcePort, maxDistance, maxXDistance, maxYDistance, rnd)
        settleX, settleY, port, fish = getSettlementLocation(startX, startY, island, world, forcePort)
        if port:
            hasPort = True
        settlements.append(Settlement(settleX, settleY, island, port, fish))
        count += 1
    return hasPort
def addSettlementsOnIsland(island, world, rnd):
    settlements = []
    #should get what I want easily 
    settlementsWanted = int(island.size / 100) + 1
    capitalX, capitalY, hasPort, hasFishing= getCapitalLocation(island, world, (settlementsWanted <= 3))
    settlements.append(Settlement(capitalX, capitalY, island, hasPort, hasFishing))
    island.top, island.bottom, island.left, island.right = islandDimensions(world, island)
    clear = 0
    for x in range(10):
        hasPort = addSettlementsTilWanted(world, island, rnd, settlementsWanted, settlements, hasPort)
        count = pruneSettlements(settlements, clear)
        if count == 0:
            break
        clear = len(settlements) - count - 1
   # addSettlementsTilWanted(world, island, rnd, settlementsWanted, settlements, hasPort)
    return settlements

def markLakes(world):
    print("Marking lakes")
    offsets = [(0,1),(1,1), (1,0),(1,-1), (-1,0),(-1,-1), (0,-1), (-1,1)]
    islandmap = utilities.copyMatrix(world.islandmap)
    for x in range(world.width):
        for y in range(world.height):
            if islandmap[x][y] == 0:
                islandmap[x][y] = -1
    
    ocean = OrderedSet()
    ocean.add((0,0))
    count = 0
    while count < len(ocean):
        x, y = ocean[count]
        if x >= 0 and y >= 0 and x < world.width and y < world.height and islandmap[x][y] == -1:
            islandmap[x][y] = 0
            for offset in offsets:
                ocean.add((x + offset[0], y + offset[1]))
        count += 1
    world.islandmapLakesMarked = islandmap
    
    
def findClosestPorts(world, portCoords, settlement, wanted, ignoreHome = True, home = True):
    close = []
    homeCount = 0
    away = 0
    paths = []
    openPaths = PriorityQueue()
    closedPaths = set()
    added = 0
    openPaths.put((0, (settlement.x, settlement.y, [])))
    while not openPaths.empty() and added < wanted:
        distance, (x, y, path) = openPaths.get()
        if (x, y) not in closedPaths:       
            for connection in world.tradeGraph[(x, y)]:
                newDistance = distance + connection.distance
                newPath = path[:]
                newPath.extend(connection.path[1:])
                newX, newY = connection.path[len(connection.path) - 1]
                openPaths.put((newDistance, (newX, newY, newPath)))
                if (newX, newY) in portCoords:
                    newSettlement = world.xySettlements[(newX, newY)] 
                    if newSettlement == settlement or (newSettlement.island == settlement.island and ignoreHome == False and home == False) or (newSettlement.island != settlement.island and ignoreHome == False and home == True):
                        continue
                    if newSettlement.island == settlement.island:
                        homeCount += 1
                    else:
                        away += 1
                    close.append(newSettlement)
                    paths.append(newPath)
                    added += 1
        closedPaths.add((x, y))
    
    
    
    return (close, homeCount, away, paths)
    
def findInbetweenRoute(world):
    offsets = [(0,1),(1,0),(0,-1),(-1,0)]
    openMap = utilities.createMatrix(world.width, world.height, lambda x, y: -1 if x == 0 or y == 0 or x == world.width - 1 or y == world.height - 1 else world.islandmap[x][y])
    initial = []
    for x in range(world.width):
        for y in range(world.height):
            if openMap[x][y] > 0:
                initial.append((x, y, openMap[x][y]))
    while len(initial) > 0:
        newInitial = []
        for location in initial:
            x, y, island = location
            if x == 0 or y == 0 or x == world.width - 1 or y == world.width - 1:
                continue
            if openMap[x][y] != island:
                #openMap[x][y] = -1
                continue
            for offset in offsets:
                offsetX = x + offset[0]
                offsetY = y + offset[1]
                #if openMap[offsetX][offsetY] != -1 and openMap[offsetX][offsetY] != island:
                if openMap[offsetX][offsetY] == 0:
                    newInitial.append((offsetX, offsetY, island))
                    openMap[offsetX][offsetY] = island
                elif openMap[offsetX][offsetY] != island:
                    openMap[offsetX][offsetY] = -1

        initial = newInitial
    return openMap
        

def getTradeRoutesForSettlement(world, settlement, portCoords):
    closest, home, away, paths = findClosestPorts(world, portCoords, settlement, 3 - max(3, len(settlement.connections)))
    for i in settlement.connections:
        if i.end.island == settlement.island:
            home += 1
        else:
            away += 1
    if away < 2:
        awayClosest, moreHome, moreAway, awayPaths= findClosestPorts(world, portCoords, settlement, 2 - away, False, False)
        closest.extend(awayClosest)
        paths.extend(awayPaths)
    for x in range(len(closest)):
        settlement.addConnection(closest[x], paths[x])
    
def startTrade(world):
    portCoords = set()
    for x in world.settlements:
        portCoords.add((x.x, x.y))
    for settlement in world.settlements:
        if settlement.port:
            getTradeRoutesForSettlement(world, settlement, portCoords)

tupleType = type((0,1))
def pathOut(world, settlement):
    deepOffsets = [(0,1),(1,0),(0,-1),(-1,0),(-1,-1),(1,1),(1,-1),(-1,1)]
    fakeOffsets = [(0,1),(1,0),(0,-1),(-1,0)]
    
    class point:
        def __eq__(self, other):
            if type(other) == tupleType:
                return (self.x, self.y) == other
            return (self.x, self.y) == (other.x, other.y)
        def __ne__(self, other):
            return self.__eq__(other)
        def __hash__(self):
            return (self.x, self.y).__hash__()
        def __init__(self, x, y, pathTo):
            self.x = x
            self.y = y
            self.pathTo = pathTo
    start = point(settlement.x, settlement.y, [(settlement.x, settlement.y)])
    expanding = set([start])
    closed = set()
    offsets = fakeOffsets
    while len(expanding) > 0:
        newExpanding = set()
        for current in expanding:
            if world.openMap[current.x][current.y] == -1:
                return current.pathTo
            if current.x == 0 or current.y == 0 or current.x == world.width - 1 or current.y == world.width - 1:
                continue
            for offset in offsets:
                offsetX = current.x + offset[0]
                offsetY = current.y + offset[1]
                if world.islandmap[offsetX][offsetY] != 0 or (offsetX, offsetY) in closed:
                    continue
                newPath = current.pathTo[:]
                newPath.append((offsetX, offsetY))
                newExpanding.add(point(offsetX, offsetY, newPath))
        closed.update(expanding)
        expanding = newExpanding
        offsets = deepOffsets
        
def getPathsOut(world):
    for x in world.settlements:
        if x.port:
            x.pathOut = pathOut(world, x)
            for place in x.pathOut:
                world.openMap[place[0]][place[1]] = -1
                
offsets = [(0,1),(1,0),(0,-1),(-1,0),(-1,1),(1,-1),(1,1),(-1,-1)]
                
def getNodePoints(world):
    nodePoints = set()
    for x in world.settlements:
        if x.port:
            nodePoints.add(x.pathOut[0])
            nodePoints.add(x.pathOut[len(x.pathOut) - 1])
    for x in range(world.width):
        for y in range(world.height):
            if world.openMap[x][y] != -1:
                continue
            neighbours = 0
            for offset in offsets:
                oX, oY = offset
                offsetX = x + oX
                offsetY = y + oY
                if offsetX <0 or offsetY < 0 or offsetX == world.width or offsetY == world.height:
                    continue
                if world.openMap[offsetX][offsetY] == -1:
                    neighbours += 1
            if neighbours > 2:
                nodePoints.add((x, y))
    return nodePoints
    
def getConnections(startPoint, otherPoints, world):
    class point:
        def __eq__(self, other):
            if type(other) == tupleType:
                return (self.x, self.y) == other
            return (self.x, self.y) == (other.x, other.y)
        def __ne__(self, other):
            return self.__eq__(other)
        def __hash__(self):
            return (self.x, self.y).__hash__()
        def __init__(self, x, y, distanceFromLast, path):
            self.x = x
            self.y = y
            self.distance = distanceFromLast
            self.path = path
    class Connection:
        def __init__(self, start, end, distance, path):
            self.start = start
            self.end = end
            self.distance = distance
            self.path = path
    x, y = startPoint
    closedBlocks = set()
    openBlocks = set()
    connections = set()
    start = point(x, y, 0, [(x, y)])
    openBlocks.add(start)
    while len(openBlocks) > 0:
        newOpen = set()
        for block in openBlocks:
            if (block.x, block.y) in otherPoints and (block.x != x or block.y != y):
                connections.add(Connection((x, y), (block.x, block.y), block.distance, block.path))
                continue
            for offset in offsets:
                offsetX = offset[0] + block.x
                offsetY = offset[1] + block.y
                if offsetX <0 or offsetY < 0 or offsetX == world.width or offsetY == world.height:
                    continue
                if world.openMap[offsetX][offsetY] == -1:
                    newPath = block.path[:]
                    newPath.append((offsetX, offsetY))
                    newOpen.add(point(offsetX, offsetY, block.distance + 1, newPath))
        closedBlocks.update(openBlocks)
        openBlocks = newOpen
        openBlocks -= closedBlocks
    return connections

def getTradeGraph(world):
    nodePoints = getNodePoints(world)
    nodeToConnections = dict()
    for node in nodePoints:
        nodeToConnections[node] = getConnections((node[0], node[1]), nodePoints, world)
    for node in nodeToConnections.iterkeys():
        for connection in nodeToConnections[node]:
            connection.start = nodeToConnections[connection.start]
            connection.end = nodeToConnections[connection.end]
    return nodeToConnections
        

def addSettlementsForWorld(world, seed):
    settlements = []
    markLakes(world)
    print("Adding settlements")
    rnd = random.Random()
    rnd.seed((seed, "settlements"))
    for x in world.islands:
        newSettlements = addSettlementsOnIsland(x, world, rnd)
        settlements.extend(newSettlements)
    world.setSettlements(settlements)

    print("Getting open map")
    openMap = findInbetweenRoute(world)
    world.openMap = openMap
    print("getting paths to open waters")
    getPathsOut(world)
    print("getting graph")
    world.tradeGraph = getTradeGraph(world)
    print("Starting trade")
    startTrade(world)