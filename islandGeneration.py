import Queue
import utilities
from ordered_set import OrderedSet

surroundingArea = [(-1,  0), (0,  1), (1,  0), (0,  -1)]

def getLandmapValue(x,  y,  landmap,  world):
    if x < 0 or y < 0 or x >= world.width or y >= world.height:
        return 0
    else:
        return landmap[x][y]

def getVisitedValue(x,  y,  visited,  world):
    if x < 0 or y < 0 or x >= world.width or y >= world.height:
        return True
    else:
        return visited[x][y]

def addsurroundingToOrderedSet(x, y, theSet, visited, landMap, world):
    for offset in surroundingArea:
        offsetX = x + offset[0]
        offsetY = y + offset[1]
        if getLandmapValue( offsetX, offsetY ,  landMap,  world) == 1:
            theSet.add((offsetX, offsetY))
            visited[x][y] = True

def depthFirstSearch(firstX,  firstY,  landmap,  world, visited,  islandValue, islandMap):
    visited[firstX][firstY] = True
    gridOrder = OrderedSet()
    gridOrder.add((firstX, firstY))
    count = 0
    while count < len(gridOrder):
        x, y = gridOrder[count]
        islandMap[x][y] = islandValue
        addsurroundingToOrderedSet(x, y, gridOrder, visited, landmap, world)
        count += 1

def findIslands(landmap,  world):
    print("Finding islands")
    visited = utilities.createMatrix(world.width,  world.height,  lambda x, y: False)
    islands = utilities.createMatrix(world.width,  world.height)
    islandNumber=0
    #island number 0 means water
    for x in range(0,  world.width):
        for y in range(0,  world.height):
            if landmap[x][y] == 1 and not visited[x][y]:
                islandNumber += 1
                depthFirstSearch(x,  y,  landmap,  world,  visited,  islandNumber,  islands)
            visited[x][y] = True
    print("Islands found")
    return (islands,  islandNumber)