import worldLoading
import getopt
import sys

class arguements:
    def __init__(self, width, height, regionsWanted, seed, toPrint, random):
        self.width = width
        self.height = height
        self.regionsWanted = regionsWanted
        self.seed = seed
        self.toPrint = toPrint
        self.random = random

def main(argv):
    print(argv)
    width = None
    height = None
    regionsWanted = None
    seed = None
    random = False
    toPrint = []
    try:
        opts, args = getopt.getopt(argv,"w:h:r:s:c",["random", "width=", "height=", "regions=", "seed=", "printLandmap", "printHeightmap", "printIslandmap", "printSizemap", "printSkinmap", "printTerritorymap", "printSettlementmap", "printRainmap", "printMaterialmap", "printFertilitymap", "printOpenmap", "printPortmap", "printCountymap", "help"])
    except getopt.GetoptError:
        print('Error getopt')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '--help':
            print 'main.py -w <width> -h <height> -r <regionWanted> -s <seed> -c <random>'
            sys.exit()
        elif opt in ("-w", "--width"):
            width = int(arg)
        elif opt in ("-h", "--height"):
            height = int(arg)
        elif opt in ("-r", "--regions"):
            regionsWanted = int(arg)
        elif opt in ("-c", "--random"):
            random = True
        elif opt in ("-s", "--seed"):
            seed = arg
        elif opt == "--printLandmap":
            toPrint.append("landmap")
        elif opt == "--printHeightmap":
            toPrint.append("heightmap")
        elif opt == "--printIslandmap":
            toPrint.append("islandmap")
        elif opt == "--printSizemap":
            toPrint.append("sizemap")
        elif opt == "--printSkinmap":
            toPrint.append("skinmap")
        elif opt == "--printTerritorymap":
            toPrint.append("territorymap")
        elif opt == "--printSettlementmap":
            toPrint.append("settlementmap")
        elif opt == "--printRainmap":
            toPrint.append("rainmap")
        elif opt == "--printMaterialmap":
            toPrint.append("materialmap")
        elif opt == "--printFertilitymap":
            toPrint.append("fertilitymap")
        elif opt == "--printOpenmap":
            toPrint.append("openmap")
        elif opt == "--printPortmap":
            toPrint.append("portmap")
        elif opt == "--printCountymap":
            toPrint.append("countymap")
        else:
            print("ERROR")
            sys.exit(2)
    worldLoading.getWorld(arguements(width, height, regionsWanted, seed, toPrint, random))
    
if __name__ == "__main__":
    main(sys.argv[1:])