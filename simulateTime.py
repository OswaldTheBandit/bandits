import math
import worldObject
import random

#will simulate the history, population, and trade for several generations

def getFarmSpot(world, settlement):
    bestSpot = (0,0)
    bestScore = -1
    sizeNeeded = 0
#   print settlement.dontfarm
    for x in range(settlement.left, settlement.right + 1):
        for y in range(settlement.top, settlement.bottom + 1):
            if world.countymap[x][y] == settlement.countyId + 1 and (x != settlement.x or y != settlement.y) and (x, y) not in settlement.farmxy and (x, y) not in settlement.dontfarm:
                fertility = world.fertility[x][y]
                distance = abs(x - settlement.x) + abs(y - settlement.y)
                score = fertility - distance * 16
                if score > bestScore:
                    bestScore = score
                    bestSpot = (x, y)
    return bestSpot
    
def addFarm(world, settlement, foodNeeded):
    if len(settlement.farms) > settlement.countySize - 3:
        return 0
    x, y = getFarmSpot(world, settlement)
    if x == 0 and y == 0:
        return 0
    fertility = world.fertility[x][y]
    bestFarm = None
    highest = 0
    possible = settlement.getTerritory().tech.items["farm"]
    for farm in possible:
        if int(farm.values["yearYield"]) > highest and int(farm.values["fertilityMin"]) <= fertility and int(farm.values["fertilityMax"]) >= fertility:
            bestFarm = farm
    farmYield = int(bestFarm.values["yearYield"])
    #foodNeeded = (2 * size + 1) ^ 2 * farmYield * 100
    #(math.sqrt(foodNeeded / (farmYield * 100)) - 1) / 2 = size
    maxSize = 3
    size = math.ceil(min(maxSize, math.ceil(math.sqrt((foodNeeded + fertility) / (farmYield * fertility + 1)) - 1) / 2))
    peasantsNeeded = int(bestFarm.values["peasantsNeeded"])
    workers = settlement.peasants.addSquad(peasantsNeeded)
    settlement.scum.directKill(peasantsNeeded)
    settlement.wealth -= math.pow((2 * size + 1), 2) * int(farm.values["cost"])
    newFarm = worldObject.Farm(x, y, size, (math.pow((2 * size + 1), 2) * farmYield - 1) * fertility, bestFarm, workers)
    settlement.farms.append(newFarm)
    world.structures.append(newFarm)
    settlement.farmxy.append((newFarm.x, newFarm.y))

    return math.floor((math.pow((2 * size + 1), 2) * farmYield * fertility - fertility) / 1)

def replaceFarm(world, settlement):
    takeAway = removeFarm(world, settlement)
    return -takeAway + addFarm(world, settlement, settlement.population * 200)

def removeFarm(world, settlement):
    if len(settlement.farms) == 0:
        return 0
    smallest = settlement.farms[0]
    smallestYield = settlement.farms[0].yearYield
    for current in settlement.farms:
        if current.yearYield < smallestYield:
            smallestYield = current.yearYield
            smallest = current
    settlement.farms.remove(smallest)
    settlement.farmxy.remove((smallest.x, smallest.y))
    settlement.peasants.moveSquad(smallest.workers, settlement.scum)
    settlement.wealth += int(math.pow((2 * smallest.size + 1), 2) * int(smallest.crop.values["cost"])) / 2
    return smallestYield
    
def determineStatistics(current):
    current.crime = 0
    current.squalor = 0
    current.squalor += max(math.ceil((float(current.population) / float(current.buildings)) - int(current.buildingType.values["maxSize"])), 0) * 400 + 100
    current.squalor += ((current.peasants.total + current.fishermen.total / 3) * 1000 ) / current.population
    current.crime += ((current.wealth + current.lastDeficit)/current.population) * (current.squalor / 100)
    current.morale = int(1000 - current.crime / 2 - current.squalor/ 6)
    current.squalor = max(min(current.squalor, 3000), 100)
    current.crime = max(min(current.crime, 1000), 100)
    current.morale = max(min(current.morale, 1000), 100)
    current.popGrow = 1.0 + max(float(current.morale - 2.0 * float(current.lastDeficit) / float(current.population)) / 10000.0, 0) - .05
    
def deteriorate(current):
    current.wealth *= (float(1000.0 - current.crime)/1000.0) * .6
    current.foodOwned = int(current.foodOwned * .5 * ((3000.0 - current.squalor) / 3000.0))
    
def getFood(current):
    fishFood = 0
    farmFood = 0
    if current.fishing:
        for squad in current.fishermen.squads:
            fishFood += squad.units * (squad.skill + 50) * 7
    
    for farm in current.farms:
       # print(farm.yearYield, farm.workers.units, (float(farm.workers.skill + 50) / 50.0))
        farmFood += int(math.floor((farm.yearYield * farm.workers.units * ((float(farm.workers.skill + 50) / 50.0)) / 60.0)))
    
    foodNeeded = current.population * 360
    
    excess = (fishFood + farmFood +  current.foodOwned) - foodNeeded
    current.foodInput = fishFood + farmFood
    current.fishFood = fishFood
    current.farmFood = farmFood
    if excess > 0:
        current.foodOwned = excess
        current.foodNeed = 0
        current.lastDeficit = 0
        current.tradeFood = 0
    if excess < 0:
        tradeFood = 0
        foodNeeded = -excess
#        print foodNeeded
        haggle = 0
        for trader in current.traders.squads:
            haggle += int(trader.training.values["haggle"]) * trader.units * (trader.skill + 50)
        deals = []
        for other in current.connections:
            otherHaggle = 0
          #  print(other.end.traders.squads)
            for trader in other.end.traders.squads:
     #           print(trader.training)
                otherHaggle += int(trader.training.values["haggle"]) * trader.units * (trader.skill + 50)
            advantage = float(haggle - otherHaggle) / float(haggle + otherHaggle + 1)
            price = 1 + advantage * .3
            deals.append((price, other.end))
        sorted(deals)
       # print(len(deals))
        for deal in deals:
            
            if foodNeeded <= 0:
                break
            price, other = deal
            price *= .1
            canBuy = int(math.floor(float(current.wealth) / price))
            theyCanSell = other.foodOwned
            willBuy = min(canBuy, theyCanSell, foodNeeded)
            expenditure = int(math.floor(price * willBuy))
            current.wealth -= expenditure
            other.wealth += expenditure
            foodNeeded -= willBuy
            tradeFood += willBuy
            other.foodOwned -= willBuy
        current.foodOwned = 0
        current.foodNeed = foodNeeded
        current.lastDeficit = foodNeeded
        current.tradeFood = tradeFood
    
    current.foodOwned

def getMoney(current):
    for craft in current.craftsmen.squads:
        craftCost = int(craft.training.values["sellPrice"])
        current.wealth += craft.units * (float(craft.skill + 50) / 50.0) * craftCost
    for trader in current.traders.squads:
        current.wealth += int((trader.units * (float(len(current.connections)) / 2.0) * (float(trader.skill + 50.0) / 50.0)) * 20)
        
def getBuildings(current):
    buildingsNeeded = int(max(math.ceil(float(current.population) / float(current.buildingType.values["maxSize"])) - current.buildings, 0))
    if buildingsNeeded > 0:
        shouldBuy = int(min(buildingsNeeded, current.wealth / int(current.buildingType.values["cost"])))
        current.wealth -= shouldBuy * int(current.buildingType.values["cost"])
        current.buildings += shouldBuy
    
def growPopulation(world, current):
    popChange = math.ceil(current.population * (current.popGrow - 1))
    current.population += popChange
    if popChange > 0:
        current.scum.addMore(popChange, 1000)
    else:
        current.scum.directKill(-popChange)
        total = current.scum.total + current.fishermen.total + current.traders.total + current.craftsmen.total + current.peasants.total
        if total > current.population:
            for x in range(20):
                total = current.scum.total + current.fishermen.total + current.traders.total + current.craftsmen.total + current.peasants.total
                if total <= current.population:
                    break
                current.fishermen.lowerTotal(current.popGrow)
                current.traders.lowerTotal(current.popGrow)
                current.craftsmen.lowerTotal(current.popGrow)
            total = current.scum.total + current.fishermen.total + current.traders.total + current.craftsmen.total + current.peasants.total
            while total > current.population and len(current.farms) > 0:
                total = current.scum.total + current.fishermen.total + current.traders.total + current.craftsmen.total + current.peasants.total
                removeFarm(world, current)
        total = current.scum.total + current.fishermen.total + current.traders.total + current.craftsmen.total + current.peasants.total
        current.scum.addMore(current.population - total, 1000)
    total = current.traders.total + current.fishermen.total + current.craftsmen.total + current.peasants.total + current.scum.total
    current.population = total
        
def recruit(current, rnd, plague):
    shouldTrain = max(int(math.floor(float(current.scum.total - 20) * .8)), 0)
    fishersToTrain = 0 if not current.fishing or plague else rnd.random() * (float(current.lastDeficit) / float(current.population)) / 50.0
    tradersToTrain = float(rnd.random() * len(current.connections))/20.0
    craftsmenToTrain = rnd.random()
    add = 0
    forEach = float(shouldTrain) / (fishersToTrain + tradersToTrain + craftsmenToTrain)
    add += current.fishermen.addMore(int(math.floor(forEach * fishersToTrain)), 10)
    add += current.traders.addMore(int(math.floor(forEach * tradersToTrain)), 10, current.trade)
    add += current.craftsmen.addMore(int(math.floor(forEach * craftsmenToTrain)), 10, current.guild)
    current.scum.directKill(add)

def setGuild(current):
    possible = current.getTerritory().tech.items["craft"]
    best=None
    profit = 0
    for x in possible:
        if x.values["sellPrice"] > profit:
            profit = x.values["sellPrice"]
            best = x
    current.guild = best
    best = None
    profit = 0
    possible = current.getTerritory().tech.items["trade"]
    for x in possible:
        value = x.values["haggle"] + x.values["steal"]
        if value > profit:
            best = x
            profit = value
    current.trade = best

def experience(current):
    current.fishermen.trainAll(1)
    current.traders.trainAll(1)
    current.craftsmen.trainAll(1)
    current.peasants.trainAll(1)

def simulateSettlements(world, plague, rnd):
    print("simulating settlements")
    for current in world.settlements:
        setGuild(current)
        determineStatistics(current)
        deteriorate(current)
        getMoney(current)
        getFood(current)
        if current.foodNeed > 0 and current.scum.total > 20 and current.wealth > 1000:
            addFarm(world, current, current.foodNeed)
        getBuildings(current)
        growPopulation(world, current)
        recruit(current, rnd, plague)
        experience(current)

def broadlySimulateYears(world, years, seed):
    rnd = random.Random()
    rnd.seed((seed, "simulateBroads"))
    for current in world.settlements:
        possible = current.getTerritory().tech.items["building"]
        current.buildingType = possible[rnd.randint(0, len(possible) - 1)]
    for year in range(years):
        print ("It is year" + str(year))
        #print world.settlements
        #i indented by accident and am lazy
        plague = False
        if year > 20 and year < 40:
            plague = True
        simulateSettlements(world, plague, rnd)