import math
import numpy

def colorMatrix(width, height, initializer = (lambda x,  y: 0)):
    matrix = numpy.empty((width, height, 3), dtype="uint8")
    for x in range(width):
         for y in range(height):
             pixel = initializer(x, y)
             for i in range(3):
                 matrix[x][y][i] = pixel[i]
    return matrix

def createMatrix(width, height, initializer = (lambda x,  y: 0)):
    return [[initializer(x, y) for y in range(height)] for x in range(width)]
    """
    matrix = numpy.empty((width, height), dtype="int16")
    for x in range(width):
         for y in range(height):
             matrix[x][y] = initializer(x, y)
    return matrix
        """

def copyMatrix(matrix):
    return [x[:] for x in matrix]
    #return numpy.copy(matrix)
    
def getColor(island):
    color = [0, 0, 0]
    if island == -1:
        return [255, 255, 255]
    elif island == 0:
        return color
    elif island < 4:
        color[island - 1] = 255
        return color
    elif island < 31:
        color[0] = ((island % 3) + 1) * 63
        color[1] = (math.floor(((island + 3) % 9)/3) + 1) * 63
        color[2] = (math.floor(((island + 3) % 27)/9) +1) * 63
        return color
    elif island < 156:
        color[0] = ((island % 5) + 1) * 50
        color[1] = (math.floor(((island + 30) % 25)/5) + 1) * 50
        color[2] = (math.floor(((island + 30) % 125)/25) +1) * 50
        return color
    elif island < 1156:
        color[0] = ((island - 155) % 10 + 1) * 22
        color[1] = (math.floor(((island - 155) % 100)/10) + 1) * 22
        color[2] = (math.floor(((island - 155) % 1000)/100) +1) * 22
        return color
    else:
        return [255, 255, 255]
