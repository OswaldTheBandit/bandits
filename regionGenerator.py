import utilities
import math

class islandRegionInfo:
    def addRegion(self, region):
        size = region.totalSize
        self.islands += region.islands
        self.x = round((self.x * self.totalSize + region.x * size) / (size + self.totalSize))
        self.y = round((self.y * self.totalSize + region.y * size) / (size + self.totalSize))
        self.totalSize += size
    def add(self, islandNumber, size, x, y):
        self.islands.append(islandNumber)
        self.x = round((self.x * self.totalSize + x * size) / (size + self.totalSize))
        self.y = round((self.y * self.totalSize + y * size) / (size + self.totalSize))
        self.totalSize += size
    def __init__(self, islandNumber, size, x, y):
        self.islands = []
        self.totalSize = 0
        self.mainNumber = islandNumber
        self.x = 0
        self.y = 0
        self.add(islandNumber, size, x, y)

def getCenters(islandMap, islandTotal, islandSize, world):
    xTotals = []
    yTotals = []
    for i in range(islandTotal):
        xTotals.append(0)
        yTotals.append(0)
    for x in range(world.width):
        for y in range(world.height):
            if islandMap[x][y] != 0:
                xTotals[islandMap[x][y] - 1] += x
                yTotals[islandMap[x][y] - 1] += y
    
    average = []
    for i in range(islandTotal):
        average.append((int(round(xTotals[i] / islandSize[i])), int(round(yTotals[i] / islandSize[i]))))
    return average

def getRequirements(islandSize, islandTotal, regionsWanted): 	
    sizeOrder = sorted(islandSize)
    smallestIndex = islandTotal - regionsWanted
    hopeAverageIndex = int(math.floor(smallestIndex / 2))
    hopeAverage = int(sizeOrder[hopeAverageIndex])
    smallest = int(sizeOrder[smallestIndex])
    medium = int(math.floor((hopeAverage * smallestIndex / regionsWanted + smallest)))
    #smallest = int(sizeOrder[islandTotal - regionsWanted] + sizeOrder[int(math.floor((islandTotal - regionsWanted) / 3))] * math.floor(islandTotal/regionsWanted))
    return [smallest, medium, 5]

def getPoints(islandMap, requirements, world, centers):
    points = []
    
    for x in range(int(math.floor(world.width/requirements[2]))):
    	    for y in range(int(math.floor(world.height/requirements[2]))):
    	    	    if islandMap[x * requirements[2]][y * requirements[2]] != 0:
    	    	        points.append((x * requirements[2], y * requirements[2], islandMap[x * requirements[2]][y * requirements[2]] - 1))
    
    for x in centers:
        points.append((x[0], x[1], islandMap[x[0]][x[1]] - 1))
    
    return points

def getRegionList(islandTotal, islandSize, centers):
    islandRegions = []
    for x in range(islandTotal):
        islandRegions.append(islandRegionInfo(x, islandSize[x], centers[x][0], centers[x][1]))
    return islandRegions

def centersToGrid(centers, islandTotal, islandToRegion, world):
    centerGrid = utilities.createMatrix(int(math.floor(world.width/50) + 1), int(math.floor(world.height/50) + 1), lambda x, y: [])
    for i in range(islandTotal):
        x = centers[i]
        closestGrid = centerGrid[int(math.floor(x[0]/50))][int(math.floor(x[1]/50))]
        closestGrid.append((x[0], x[1], islandToRegion[i]))
    return centerGrid

def findClosestCenterInGrid(region, centerGrid, islandToRegion, world, maxSize, regionList):
    closestDistance = 50965096
    closestRegion = -1
    closeCenters = []
    smallestX = int(0 if region.x < 50 else math.floor(region.x/50) - 1)
    largestX = int(math.floor(region.x/50) if math.floor(region.x/50) == math.floor((world.width - 1)/50) else math.floor(region.x/50) + 1)
    smallestY = int(0 if region.y < 50 else math.floor(region.y/50) - 1)
    largestY = int(math.floor(region.y/50) if math.floor(region.y/50) == math.floor((world.height - 1)/50) else math.floor(region.y/50) + 1)
    for x in range(smallestX, largestX + 1):
        for y in range(smallestY, largestY + 1):
            closeCenters.extend(centerGrid[x][y])
            
    for x in closeCenters:
        if regionList[islandToRegion[x[2]]].totalSize < maxSize and islandToRegion[x[2]] != region.mainNumber:
            xDistance = x[0] - region.x
            yDistance = x[1] - region.y
            distance = (xDistance * xDistance + yDistance * yDistance) * max([regionList[islandToRegion[x[2]]].totalSize, region.totalSize])
            if distance  < closestDistance:
                closestDistance = distance
                closestRegion = islandToRegion[x[2]]
    return (closestDistance, closestRegion)

def findClosestInGrid(region, maxSize, world, pointDistance, islandMap, spread, islandToRegion, regionList):
    closestDistance = 509650965096
    closestRegion = -1
    smallestX = int(0 if region.x - spread < 0 else region.x - spread)
    largestX = int(world.width - 1 if region.x + spread >= world.width else region.x + spread)
    smallestY = int(0 if region.y - spread < 0 else region.y - spread)
    largestY = int(world.height - 1 if region.y + spread >= world.height else region.y + spread)
    for x in range(smallestX, largestX, pointDistance):
        for y in range(smallestY, largestY, pointDistance):
            pointRegion = islandMap[x][y] - 1
            if pointRegion != -1 and regionList[islandToRegion[pointRegion]].totalSize < maxSize and islandToRegion[pointRegion] != region.mainNumber:
                xDistance = x - region.x
                yDistance = y - region.y
                distance = (xDistance * xDistance + yDistance * yDistance) * max([regionList[islandToRegion[pointRegion]].totalSize, region.totalSize])
                if distance < closestDistance:
                    closestDistance = distance
                    closestRegion = islandToRegion[pointRegion]
    return (closestDistance, closestRegion)

def findClosest(region, maxSize, islandToRegion, islandMap, centerGrid, world, requirements, regionList):
    #larger than any practical map size square
    closestDistance = 50965096
    closestRegion = -1
    centersClosest = findClosestCenterInGrid(region, centerGrid, islandToRegion, world, maxSize, regionList)
    pointsClosest = findClosestInGrid(region, maxSize, world, requirements[2], islandMap, 70, islandToRegion, regionList)
    if centersClosest[0] < pointsClosest[0]:
        closestDistance = centersClosest[0]
        closestRegion = centersClosest[1]
    else:
        closestDistance = pointsClosest[0]
        closestRegion = pointsClosest[1]
    return (closestDistance, closestRegion)

def combineRegions(regionToRemove, regionToRemain, islandToRegion):
    regionToRemain.addRegion(regionToRemove)
    for replace in regionToRemove.islands:
        islandToRegion[replace] = regionToRemain.mainNumber

def mergeRegions(centerGrid, islandRegions, islandToRegion, largeMultiplier, islandMap, requirements, world):
    needToMerge = []
    canBeMerged = []
    avoidMerge = []
    for x in islandRegions:
        if x.totalSize < requirements[0]:
            needToMerge.append(x.mainNumber)
            canBeMerged.append(x.mainNumber)
        elif x.totalSize < requirements[1]:
            canBeMerged.append(x.mainNumber)
        else:
            avoidMerge.append(x.mainNumber)
    
    #now merge
    
    for x in needToMerge:
        #check if x is too large to need merging
        if islandRegions[islandToRegion[x]].totalSize > requirements[0] or islandToRegion[x] != x:
            continue
        currentRegion = islandRegions[islandToRegion[x]]
        closestSmall = findClosest(currentRegion, requirements[1], islandToRegion, islandMap, centerGrid, world, requirements, islandRegions)
        closestLarge = findClosest(currentRegion, 10000, islandToRegion, islandMap, centerGrid, world, requirements, islandRegions)
        mergeNumber = None
        if closestLarge[0] * largeMultiplier * largeMultiplier < closestSmall[0]:
            mergeNumber = closestLarge[1]
        else:
            mergeNumber = closestSmall[1]
        combineRegions(currentRegion, islandRegions[mergeNumber], islandToRegion)
        
def getCorrectedRegionList(islandToRegion, regionList, islandTotal):
    correctedRegionListNumber = []
    total = 0
    newIslandToRegion = []
    for x in range(islandTotal):
        if islandToRegion[x] == x:
            newIslandToRegion.append(total)
            total += 1
            correctedRegionListNumber.append(x)
        else:
            newIslandToRegion.append(islandToRegion[x])
    for x in range(islandTotal):
        islandToRegion[x] = newIslandToRegion[islandToRegion[x]]
    newRegionList = []
    
    for x in correctedRegionListNumber:
        newRegionList.append(regionList[x])
    for x in range(total):
        newRegionList[x].mainNumber = x
    return newRegionList
    
def findRegions(islandMap, islandTotal, islandSize, world, regionsWanted):
    tooBigModifier = 2
    
    requirements = getRequirements(islandSize, islandTotal, regionsWanted)
    
    islandRegions = []
    islandToRegion = [x for x in range(islandTotal)]
    
    centers = getCenters(islandMap, islandTotal, islandSize, world)
    
    centerGrid = centersToGrid(centers, islandTotal, islandToRegion, world)
    
    
    
    islandRegions = getRegionList(islandTotal, islandSize, centers)
    
    mergeRegions(centerGrid, islandRegions, islandToRegion, tooBigModifier, islandMap, requirements, world )
    
    islandRegions = getCorrectedRegionList(islandToRegion, islandRegions, islandTotal)
    
    regionMap = utilities.createMatrix(world.width, world.height, lambda x,y: 0 if islandMap[x][y] == 0 else islandToRegion[islandMap[x][y] - 1] + 1)
    print("Regions found")
    return (islandToRegion, regionMap, centers, len(islandRegions))
    