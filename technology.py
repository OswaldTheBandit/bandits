import math
import random
#will determine the technology levels for various territories based on size and total islands


class Technology:
    def setByIndex(self, index, value):
        if index == 0:
            self.meleeWeapons += value
        elif index == 1:
            self.armor += value
        elif index == 2:
            self.rangedWeapons += value
        elif index == 3:
            self.artillery += value
        elif index == 4:
            self.building += value
        elif index == 5:
            self.trade += value
        elif index == 6:
            self.mining += value
        elif index == 7:
            self.craft += value
        elif index == 8:
            self.farm += value
        elif index == 9:
            self.training += value
        elif index == 10:
            self.rallying += value
        self.pointsLeft -= value
    
    def printValues(self):
        print("Skin %s, Points %s, boats %s, melee %s, armor %s, ranged %s, artillery %s, building %s, trade %s, mining %s, craft %s, farm %s, training %s, rallying %s"
        	% (self.skin, self.points, self.boats, self.meleeWeapons, self.armor, self.rangedWeapons,
        		self.artillery, self.building, self.trade, self.mining, self.craft, self.farm,
        		self.training, self.rallying))
    
    def __init__(self, points):
        self.points = points
        self.boats = 0
        self.meleeWeapons = 0
        self.armor = 0
        self.rangedWeapons = 0
        self.artillery = 0
        self.building = 0
        self.trade = 0
        self.mining = 0
        self.craft = 0
        self.farm = 0
        self.training = 0
        self.rallying = 0
        self.pointsLeft = points
        self.skin = 0

def getBoatPoints(territory, tech, maxIslands):
    for x in range(len(tech)):
        boatPoints = int(math.ceil(70 * float(len(territory[x].islands)) / float(maxIslands)))
        tech[x].boats = boatPoints
        tech[x].pointsLeft -= boatPoints

def distributeInitialPoints(territory, tech, smallestSize, largestSize, largestIslands):
    smallestTech = 300
    largestTech = 600
    for x in territory:
        if x.size < smallestSize:
            smallestSize = x.size
        if len(x.islands) > largestIslands:
            largestIslands = len(x.islands)
        if x.size > largestSize:
            largestSize = x.size
    if largestSize - smallestSize == 0:
        techPointsPerSize = 0
    else:
        techPointsPerSize = - float(largestTech - smallestTech) / float(largestSize - smallestSize)
    print(smallestSize, largestSize, largestIslands, techPointsPerSize)
    for x in territory:
        tech.append(Technology(int(round(((x.size - smallestSize) * techPointsPerSize) + largestTech))))
    getBoatPoints(territory, tech, largestIslands)

def randomlyDistributeTech(tech, rnd):
    values = [16, 15, 14, 14, 13, 11, 10, 9, 8, 7, 5]
    total = 0
    for x in values:
        total += x
    for current in tech:
        rnd.shuffle(values)
        partsPerValue = float(current.pointsLeft) / float(total)
        largeIndex = 0
        for x in range(11):
            if values[x] == 16:
                largeIndex = x
            current.setByIndex(x, int(math.ceil(partsPerValue * values[x])))
        current.setByIndex(largeIndex, current.pointsLeft)
  
#the tech skins are highlander, french, moors, oriental
def setSkins(tech):
    for x in tech:
        if x.points < 400:
            x.skin = 0
        elif x.points < 500:
            x.skin = 1
        elif x.points < 550:
            x.skin = 2
        else:
            x.skin = 3

def getTechPoints(territory, seed):
    rnd = random.Random()
    rnd.seed((seed, "tech"))
    tech = []
    smallestSize = 10000000
    largestSize = 0
    largestIslands = 0
    distributeInitialPoints(territory, tech, smallestSize, largestSize, largestIslands)
    randomlyDistributeTech(tech, rnd)
    setSkins(tech)
    for x in tech:
        x.printValues()
    return tech