import utilities
import Queue
import worldObject


def doesRowContainCounty(world, islandMapId, row, start, finish):
    for x in range(start, finish + 1):
        if world.countymap[x][row] == islandMapId:
            return True
    return False

def doesColumnContainCounty(world, islandMapId, column, start, finish):
    for x in range(start, finish + 1):
        if world.countymap[column][x] == islandMapId:
            return True
    return False

def countyDimensions(world, settlement):
    count = 0
    while doesRowContainCounty(world, settlement.countyId + 1, settlement.y - count, settlement.island.left, settlement.island.right):
        count += 1
    top = settlement.y - count
    count = 0
    while doesRowContainCounty(world, settlement.countyId + 1, settlement.y + count, settlement.island.left, settlement.island.right):
        count += 1
    bottom = settlement.y + count
    count = 0
    while doesColumnContainCounty(world, settlement.countyId + 1, settlement.x - count, top, bottom):
        count += 1
    left = settlement.x - count
    count = 0
    while doesColumnContainCounty(world, settlement.countyId + 1, settlement.x + count, top, bottom):
        count += 1
    right = settlement.x + count
    return (top, bottom, left, right)


def getCountymap(world):
    print("Getting counties")
    offsets = [(0,1),(1,0),(-1,0),(0,-1)]
    countymap = utilities.createMatrix(world.width, world.height)
    openBlocks = set()
    closedBlocks = set()
    for current in world.settlements:
        openBlocks.add((current.x, current.y, current))
    while len(openBlocks) > 0:
        newOpen = set()
        for block in openBlocks:
            x, y, settlementCounty = block
            countyId = settlementCounty.countyId
            for offset in offsets:
                offsetX = x + offset[0]
                offsetY = y + offset[1]
                if offsetX < 0 or offsetY < 0 or offsetX >= world.width or offsetY >= world.height:
                    continue
                if countymap[offsetX][offsetY] == 0 and world.heightmap[offsetX][offsetY] >= 0:
                    newOpen.add((offsetX, offsetY, settlementCounty))
                    settlementCounty.countySize += 1
                    countymap[offsetX][offsetY] = countyId + 1
                elif countymap[offsetX][offsetY] != settlementCounty.countyId + 1 and world.heightmap[offsetX][offsetY] >= 0:
                    otherCounty = world.settlements[countymap[offsetX][offsetY] - 1]
                    otherCounty.landNeighbours.add(settlementCounty)
                    settlementCounty.landNeighbours.add(otherCounty)
                    
        closedBlocks.update(openBlocks)
        openBlocks = newOpen
    world.countymap = countymap
    for current in world.settlements:
        current.top, current.bottom, current.left, current.right = countyDimensions(world, current)
        

def buildRoads(world):
    print("Road building")
    offsets = [(0,1),(-1,1),(1,0),(1,-1),(-1,0),(-1,-1),(0,-1),(1,1)]
    for current in world.settlements:
        for neighbour in current.landNeighbours:
            if neighbour in current.done:
                continue
            openBlocks = Queue.PriorityQueue()
            closedBlocks = set()
            start = (0, current.x, current.y, [(current.x, current.y)], 0)
            openBlocks.put(start)
            while not openBlocks.empty() and not openBlocks.full():
                heuristic, x, y, path, distance= openBlocks.get()
                if (x, y) in closedBlocks:
                    continue
                closedBlocks.add((x, y))
                if (x, y) == (neighbour.x, neighbour.y):
                    current.addConnection(neighbour, path)
                    current.done.add(neighbour)
                    neighbour.done.add(current)
                    
                    current.dontfarm.update(set(path))
                    neighbour.dontfarm.update(set(path))
                    break
                
                if world.heightmap[x][y] < 0 or world.countymap[x][y] not in [neighbour.countyId + 1, current.countyId + 1]:
                    continue
                
                for offset in offsets:
                    oX, oY = offset
                    offsetX = oX + x
                    offsetY = oY + y
                    manhattan = abs(offsetX - neighbour.x) + abs(offsetY - neighbour.y)
                    newPath = path[:]
                    newPath.append((offsetX, offsetY))
                    openBlocks.put((distance + (1 if oX == 0 or oY == 0 else 1.4) + manhattan * 2, offsetX, offsetY, newPath, distance))
                
                