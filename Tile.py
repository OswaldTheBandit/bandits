import random
import sys

class Tile:
    width = int(33)
    height = int(33)
    def printVariance(self):
        for i in range(self.height):
            for q in range(self.width):
                sys.stdout.write(("%s" % self.heightmapVariance[i][q]).rjust(4))
            sys.stdout.write("\n")
        
    def diamondSquare(self, x,  y,  depth,  baseHeight):
        min = -baseHeight if (baseHeight - depth) < 0 else (-depth)
        self.heightmapVariance[y][x] = random.randint(min,  depth)
    def getHeightmap(self,  x, y):
        return self.heightmapVariance[y][x]
    def __init__(self, xLocation,  yLocation, isSettlement, structures, height, seed,  settlementInfo = None):
        print("Generating tile...")
        random.seed(seed)
        self.x = xLocation
        self.y = yLocation
        self.hasSettlement = isSettlement
        if self.hasSettlement:
            self.settlement = settlementInfo
        self.heightmapVariance = [[0 for i in range(self.width)] for q in range(self.height)] 
        self.biome = [[0 for i in range(self.width)] for q in range(self.height)] 
        self.structures=structures
        self.diamondSquare(self.width//2,  self.height//2,  self.width//2, 0 + height)
        depth = int(self.width//2)
        while depth > 1 :
            depth = depth // 2
            #diamond step
            i = int(depth)
            while i < self.width-depth:
                q = int(depth)
                while q < self.height-depth:
                    total = self.getHeightmap(i - depth,  q - depth)
                    total += self.getHeightmap(i + depth,  q - depth)
                    total += self.getHeightmap(i - depth,  q + depth)
                    total += self.getHeightmap(i + depth,  q + depth)
                    average = round(total/4)
                    self.diamondSquare(i, q,  depth, average + height)
                    q += depth * 2
                i += depth * 2
                
            #first round of square step
            i = int(depth * 2)
            while i < self.width - depth:
                q = int(depth)
                while q < self.height-depth:
                    total = self.getHeightmap(i - depth,  q)
                    total += self.getHeightmap(i + depth,  q )
                    total += self.getHeightmap(i,  q - depth)
                    total += self.getHeightmap(i,  q + depth)
                    self.diamondSquare(i, q,  depth, average + height)
                    average = round(total/4)
                    q += depth * 2
                i += depth * 2
            #second square step
            i = int(depth)
            while i < self.width - depth:
                q = int(depth * 2)
                while q < self.height-depth:
                    total = self.getHeightmap(i - depth,  q)
                    total += self.getHeightmap(i + depth,  q )
                    total += self.getHeightmap(i,  q - depth)
                    total += self.getHeightmap(i,  q + depth)
                    self.diamondSquare(i, q,  depth, average + height)
                    average = round(total/4)
                    q += depth * 2
                i += depth * 2
        #now smooth the heightmap
        for i in range(2):
            for i in range(1, self.width-1):
                 for q in range(1, self.height-1):
                     total = self.getHeightmap(i-1,  q-1)
                     total += self.getHeightmap(i-1,  q)
                     total += self.getHeightmap(i-1,  q+1)
                     total += self.getHeightmap(i,  q+1)
                     total += self.getHeightmap(i+1,  q+1)
                     total += self.getHeightmap(i+1,  q)
                     total += self.getHeightmap(i+1,  q-1)
                     total += self.getHeightmap(i,  q-1)
                     average = total/8
                     if abs(average -  self.getHeightmap(i,  q)) > 2:
                         if average -  self.getHeightmap(i,  q) > 0:
                             self.heightmapVariance[q][i] = round(average) - 2
                         else:
                            self.heightmapVariance[q][i]=round(average) + 2
        print("Tile Generated")
